# [1.2.0](https://gitlab.com/depixy/storage/compare/v1.1.0...v1.2.0) (2020-07-23)


### Features

* add delete ([f9f6ea9](https://gitlab.com/depixy/storage/commit/f9f6ea97ef5ea7120a04ec4ab3bb7e70024ce2fd))
* add deleteMany ([38b033b](https://gitlab.com/depixy/storage/commit/38b033bf665156834e8d20416f8e8fe933e3725e))

# [1.1.0](https://gitlab.com/depixy/storage/compare/v1.0.2...v1.1.0) (2020-07-21)


### Features

* add metadata support ([7fb61f9](https://gitlab.com/depixy/storage/commit/7fb61f976546a4f267f7f4ac6a323695a2043eb8))

## [1.0.2](https://gitlab.com/depixy/storage/compare/v1.0.1...v1.0.2) (2020-07-20)


### Bug Fixes

* accept Readable ([704095c](https://gitlab.com/depixy/storage/commit/704095c1066dfb490bd512389395fea1766bd894))

## [1.0.1](https://gitlab.com/depixy/storage/compare/v1.0.0...v1.0.1) (2020-07-20)


### Bug Fixes

* change publishConfig to public ([d2cdef3](https://gitlab.com/depixy/storage/commit/d2cdef3766d6a7ee3cfd3cc25db3ca4da1ac0826))

# 1.0.0 (2020-07-20)


### Bug Fixes

* add missing devDependencies ([31ff791](https://gitlab.com/depixy/storage/commit/31ff791701d9ab6c1131b1b3644ab2c781166d4f))
* npm scripts ([ec3e23f](https://gitlab.com/depixy/storage/commit/ec3e23f74b445bc4d87482e2e9a30cfd396ecb19))


### Features

* initial commit ([553e6b0](https://gitlab.com/depixy/storage/commit/553e6b0c62000d71ff32b8a9160a7a0838f99140))
