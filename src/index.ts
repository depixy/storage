import { Readable } from "stream";

export interface Storage {
  save(
    key: string,
    stream: Readable,
    metadata?: Record<string, any>
  ): Promise<void>;
  load(key: string): Promise<Readable>;
  exist(key: string): Promise<boolean>;
  metadata(key: string): Promise<Record<string, any>>;
  delete(key: string): Promise<void>;
  deleteMany(keys: string[]): Promise<void>;
}
