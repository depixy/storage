# @depixy/storage

[![License][license_badge]][license] [![Pipelines][pipelines_badge]][pipelines] [![Coverage][coverage_badge]][pipelines] [![NPM][npm_badge]][npm] [![semantic-release][semantic_release_badge]][semantic_release]

Storage interface for Depixy.

## Installation

```
npm i @depixy/storage
```

[license]: https://gitlab.com/depixy/storage/blob/master/LICENSE
[license_badge]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[pipelines]: https://gitlab.com/depixy/storage/pipelines
[pipelines_badge]: https://gitlab.com/depixy/storage/badges/master/pipeline.svg
[coverage_badge]: https://gitlab.com/depixy/storage/badges/master/coverage.svg
[npm]: https://www.npmjs.com/package/@depixy/storage
[npm_badge]: https://img.shields.io/npm/v/@depixy/storage/latest.svg
[semantic_release]: https://github.com/semantic-release/semantic-release
[semantic_release_badge]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
